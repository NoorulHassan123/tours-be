const express = require("express");
const morgan = require("morgan");
const app = express();
const path = require("path");
const rateLimit = require("express-rate-limit");
const helmet = require("helmet");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const hpp = require("hpp");
const appError = require("./utils/appError");
const globalErrorHanlder = require("./utils/globalErrorHandler");
const userRouter = require("./routes/userRouter");
const tourRouter = require("./routes/tourRouter");
const reviewRouter = require("./routes/reviewRouter");
const viewRouter = require("./routes/viewRoutes");
const bookingRouter = require("./routes/bookingRoutes");
const bookingController = require("./controllers/bookingController");
const bodyParser = require("body-parser");

const cors = require("cors");

console.log(process.env.NODE_ENV);

app.set("view engine", "pug");

app.set("views", path.join(__dirname, "views"));

// serving static files 
app.use(express.static(path.join(__dirname, "public")));

app.use(cors());
// for security
app.use(helmet());
app.use("/webhook-checkout", express.raw({ type: 'application/json' }), bookingController.webhookCheckout);

const limiter = rateLimit({
  max: 100,
  windowMs: 60 * 60 * 1000,
  message: "Too,many requests from this IP please try again later!",
});
// limiting multiple requests from same IP
app.use("/api", limiter);

app.use(express.json());

app.use(mongoSanitize());
app.use(xss());
app.use(hpp());

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// view Routes/client side routes 
app.use("/", viewRouter);


// tours routes
app.use("/api/v1/tours", tourRouter);
// users routes
app.use("/api/v1/users", userRouter);
// review routes 
app.use("/api/v1/reviews", reviewRouter);
// booking Routes
app.use("/api/v1/bookings", bookingRouter);

// app.use(express.json({
//   verify: (req, res, buffer) => req['rawBody'] = buffer
// }));
// app.use("/webhook-checkout", express.raw({ type: 'application/json' }), bookingController.webhookCheckout);
// app.use(express.raw({ type: "*/*" }))

// handling unwanted routes
app.all("*", (req, res, next) => {
  next(new appError(`Can't find ${req.originalUrl} on this server `, 404));
});

// global error handler
app.use(globalErrorHanlder);

module.exports = app;
