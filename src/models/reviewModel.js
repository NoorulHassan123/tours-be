const mongoose = require('mongoose');
const tour = require("../models/tourModel");

const reviewSchema = new mongoose.Schema({
    review: {
        type: String,
        required: [true, 'review field cant be empty',]
    },
    rating: {
        type: Number,
        min: 1,
        max: 5
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    tour: {
        type: mongoose.Schema.ObjectId,
        ref: "Tour",
        required: [true, "review must belong to the specific tour"]
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: "User",
        required: [true, "review must belong to the specific user"]
    }
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true },
    }
);
reviewSchema.index({ tour: 1, user: 1 }, { unique: true });

// reviewSchema.pre(/^find/, function () {
//     this.populate({
//         path: "tour",
//         select: 'place'
//     }).populate({
//         path: "user",
//         select: "name photo"
//     })
// })
reviewSchema.pre(/^find/, function () {
    this.populate({
        path: "user",
        select: "name photo"
    })
})

reviewSchema.statics.calcAverageRatings = async function (tourId) {
    const stats = await this.aggregate([
        {
            $match: { tour: tourId },
        },
        {
            $group: {
                _id: "$tour",
                nRating: { $sum: 1 },
                avgRating: { $avg: "$rating" }
            }
        }
    ])
    console.log("stats", stats);

    await tour.findByIdAndUpdate(tourId, {
        ratingsQuantity: stats[0].nRating,
        ratingsAverage: stats[0].avgRating
    });
}

reviewSchema.post("save", function () {
    this.constructor.calcAverageRatings(this.tour);

})
const Review = new mongoose.model('Review', reviewSchema);
module.exports = Review;