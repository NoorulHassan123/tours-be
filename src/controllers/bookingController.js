const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const Tour = require("../models/tourModel");
const User = require("../models/userModel");
const Booking = require("../models/bookingModel");
const catchAsync = require("../utils/catchAsync");

exports.getCheckOutSession = catchAsync(async (req, res, next) => {
    const tour = await Tour.findById(req.params.tourID);

    const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        success_url: `${process.env.SUCCESS_PAYMENT_URL}`,
        cancel_url: `${process.env.CANCEL_PAYMENT_URL}tours/${req.params.tourID}`,
        customer_email: req.user.email,
        client_reference_id: req.params.tourID,

        line_items: [
            {
                name: `${tour.place} Tour`,
                description: tour.discription,
                images: [tour.image],
                amount: tour.fee * 100,
                currency: 'usd',
                quantity: 1
            }
        ]
    })
    res.status('200').json({
        status: 'success',
        session
    })
});

// exports.createCheckoutBooking = catchAsync(async (req, res, next) => {
//     console.log('i am called')
//     const { user, tour, price } = req.query;
//     if (!user && !tour && !price) return next();
//     await Booking.create({ tour, user, price });
//     res.redirect('http://localhost:3000/');
// })


const createCheckoutBooking = async (session) => {
    console.log("session", session)
    console.log("createCheckoutBooking called")
    const tour = session.client_reference_id;
    const user = (await User.findOne({ email: session.customer_email })).id;
    // console.log("session.line_items[0]", session.line_items[0])
    // console.log("session.line_items[0]", session.display_items[0])
    // const price = session.display_items[0].amount / 100;
    const price = session.amount_total / 100;
    console.log("tour user price", tour, user, price)
    await Booking.create({ tour, user, price });
}

exports.webhookCheckout = (req, res, next) => {
    console.log("i am called .....")
    let event;
    try {
        const signature = req.headers['stripe-signature'];
        console.log("signature", signature)
        event = stripe.webhooks.constructEvent(
            req.body,
            signature,
            'whsec_rfVhkxQLglIK7VugDZ27Ym0pqPos6jN7'
        )
    } catch (error) {
        return res.status(400).send(`Webhook error:${error.message}`)
    }
    if (event.type === 'checkout.session.completed') {
        createCheckoutBooking(event.data.object)
    }
    res.status(200).json({ received: true })
}