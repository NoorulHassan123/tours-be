const mongoose = require('mongoose');
const tour = require("../models/tourModel");

const bookingSchema = new mongoose.Schema({
    tour: {
        type: mongoose.Schema.ObjectId,
        ref: 'Tour',
        required: [true, 'booking belongs to the tour']
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: [true, 'booking belongs to the user']
    },
    price: {
        type: Number,
        required: [true, 'booking must have a price']
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    paid: {
        type: Boolean,
        default: true,
    },
}
);


bookingSchema.pre(/^find/, function (next) {
    this.populate('user').populate({
        path: 'tour',
        select: 'place'
    })
    next();
})
const Booking = new mongoose.model('Booking', bookingSchema);
module.exports = Booking;