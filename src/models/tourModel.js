const mongoose = require("mongoose");
// const user = require("./userModel")
const tourSchema = new mongoose.Schema(
  {
    place: {
      type: String,
      required: true,
    },
    fee: {
      type: Number,
      required: true,
    },
    lastdate: {
      type: String,
      required: true,
    },
    maxGroupSize: {
      type: Number,
      required: true,
    },
    ratingsAverage: {
      type: Number,
      required: true,
      default: 4.5,
      set: val => Math.round(val * 10) / 10 //4.666, 46.66, 47, 4.7 
    },
    ratingsQuantity: {
      type: Number,
      required: true,
      default: 0,
    },
    discription: {
      type: String,
      reuired: true,
    },
    secretTour: {
      type: Boolean,
      default: false,
    },
    startLocation: {
      type: {
        type: String,
        default: 'Point',
        enum: ['Point']
      },
      cordinates: [Number],
      address: String,
      discription: String
    },
    locations: [
      {
        type: {
          type: String,
          default: 'Point',
          enum: ['Point']
        },
        cordinates: [Number],
        address: String,
        discription: String,
        day: Number
      }
    ],
    guides: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
      }
    ],
    image: {
      type: String,
      required: true,
    },
    images: [String],

    createdAt: {
      type: Date,
      default: Date.now(),
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

tourSchema.index({ fee: 1, ratingsAverage: -1 })
tourSchema.index({ slug: -1 })
// virtual properties
tourSchema.virtual("weeklyRating").get(function () {
  return this.ratingsQuantity / 7;
});
// document middleware
tourSchema.pre("save", function (next) {
  // this is the currently document that has to be saved
  console.log(this);
  next();
});
// query middleware
tourSchema.pre("find", function () {
  this.find({ secretTour: { $ne: true } });
});

// before creating and saving tour into DB finding guides documents with their ids and embedding / saving guides documents  along with tours documents but the drawback of this is when thour guide update his data e.g:email,name then we have to check wheather this guide is in our tour guides field if so then we have to also update here so we dont' gonna user this we use populate function inside the query middleware 

// tourSchema.pre('save', async function (next) {
//   const guidesPromises = this.guides.map(async (id) => await user.findById(id));
//   this.guides = await Promise.all(guidesPromises);
//   next();
// });


// populating guides field in tour model  
tourSchema.pre(/^find/, function (next) {
  this.populate({
    path: "guides",
    select: '-__v -passwordChangedAt'
  });
  next();
})

// virtual populating 
tourSchema.virtual("reviews", {
  ref: "Review",
  foreignField: "tour",
  localField: "_id"
})

const Tour = mongoose.model("Tour", tourSchema);
module.exports = Tour;
