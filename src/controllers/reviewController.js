const Review = require("../models/reviewModel");
const catchAsync = require("../utils/catchAsync");
const factory = require("./handlerFactory");

exports.setTourUserIds = (req, res, next) => {
    if (!req.body.tour) req.body.tour = req.params.tourId;
    if (!req.body.user) req.body.user = req.user.id;
    next();
}

// create review
exports.createReview = factory.createOne(Review);

// get all reviews      
exports.getAllReviews = factory.getAll(Review);

// get review 
exports.getReview = factory.getOne(Review);

// delete review 
exports.deleteReview = factory.deleteOne(Review);

// update Review
exports.updateReview = factory.updateOne(Review);
