const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "provide your name"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "provide your email"],
    lowercase: true,
    validate: [validator.isEmail, "plz provide a valid email"],
  },
  phone: {
    type: Number,
    // required: [true, "provide your phone"],
  },
  address: {
    type: String,
    // required: [true, "provide your phone"],
  },
  photo: {
    type: String,
    default: "default.png"
  },
  role: {
    type: String,
    enum: ["user", "guide", "leade-guide", "admin"],
    default: "user",
  },
  socialUserId: {
    type: String,
  },
  password: {
    type: String,
    // required: [true, "plz provide a password"],
    minlength: 8,
    select: false,
  },
  confirmPassword: {
    type: String,
    // required: [true, "plz confirm your password"],
    validate: {
      validator: function (el) {
        return el === this.password;
      },
      message: "passwords are not same",
    },
  },
  active: {
    type: Boolean,
    default: true,
    select: false,
  },
  passwordResetToken: String,
  passwordResetExpires: Date,
});

// hashing password
userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.password = await bcrypt.hash(this.password, 10);
  this.confirmPassword = undefined;
  next();
});

// bcrypt compare password
userSchema.methods.correctPassword = async function (userPassword, password) {
  return await bcrypt.compare(userPassword, password);
};

// generate random passwordReset token
userSchema.methods.randomPasswordResetToken = async function () {
  const resetToken = crypto.randomBytes(32).toString("hex");
  this.passwordResetToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");
  this.passwordResetExpires = Date.now() + 10 * 60 * 1000;
  return resetToken;
};

// filtering the users which are active
userSchema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } });
  next();
});

const User = mongoose.model("User", userSchema);
module.exports = User;
