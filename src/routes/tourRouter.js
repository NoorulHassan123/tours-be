const express = require("express");
const toursController = require("../controllers/toursController");
const authController = require("../controllers/authController");
const reviewRoutes = require("../routes/reviewRouter");
const bookingConroller = require("../controllers/bookingController");
// const reviewController = require("../controllers/reviewController");


const Router = express.Router();

Router.route("/")
  .get(toursController.getAllTours)
  .post(authController.protect, authController.restrictTo("admin", "lead-guide"), toursController.addTour);

Router.route("/myBookedTours")
  .get(authController.protect, toursController.getMyTours)

// if url contains id then this middleware called
// Router.param("id", toursController.checklId);

Router.route("/top-5-cheap").get(
  toursController.aliasTopTours,
  toursController.getAllTours
);

Router.route("/tour-stats").get(toursController.getTourStats);

Router.route("/:id", toursController.getTour)
  .get(toursController.getTour)
  .patch(
    authController.protect,
    authController.restrictTo("admin", "lead-guide"), toursController.upDateTour)
  .delete(
    authController.protect,
    authController.restrictTo("admin", "lead-guide"),
    toursController.deleteTour
  );

//  nested routes for tour reviews 
Router.use("/:tourId/reviews", reviewRoutes)

module.exports = Router;
