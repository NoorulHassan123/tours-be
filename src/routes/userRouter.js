const express = require("express");
const Router = express.Router();
const userController = require("../controllers/userController");
const authController = require("../controllers/authController");
const { multerUploadS3 } = require("../utils/multer");
const router = require("./reviewRouter");


// const upload = multer({ dest: '../uploads/usersImages' });

Router.post("/signup", authController.signUp);
Router.post("/login", authController.logIn);
Router.post("/social/login", authController.createSocialUser);


Router.get("/me", authController.protect, userController.getMe, userController.getUser)

Router.post("/forgotPassword", authController.forgotPassword);
Router.patch("/resetPassword/:token", authController.resetPassword);
// Router.patch("/updateMe", multer.uploadUserPhoto, multer.resizeUserPhoto, userController.updateMe);

Router.patch("/updateMe", authController.protect, multerUploadS3.any(), userController.updateMe);
Router.delete("/deleteMe", authController.protect, userController.deleteMe);

Router.patch(
  "/updatePassword",
  authController.protect,
  authController.updatePassword
);

router.use(authController.restrictTo("admin"));
Router.use(authController.protect);

Router.route("/")
  .get(userController.getAllUsers)
  .post(userController.createUser);

// for admin routes
Router.route("/:id")
  .get(userController.getUser)
  .delete(userController.deleteUser)
  .patch(userController.updateUser)

module.exports = Router;