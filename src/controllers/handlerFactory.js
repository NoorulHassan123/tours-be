const catchAsync = require("../utils/catchAsync");
const appError = require("../utils/appError");
const ApiFeatuers = require("../utils/ApiFeatures");

// delete doc
exports.deleteOne = Model => catchAsync(async (req, res, next) => {
    // for deleting Review you have to send query like this
    // const doc = await Model.findByIdAndDelete(req.params.tourId);
    const doc = await Model.findByIdAndDelete(req.params.id);
    if (!doc) {
        return next(new appError("document with this ID not Exist", 404));
    }
    res.status(200).json({
        success: "deleted",
    });
});

// update doc
exports.updateOne = Model => catchAsync(async (req, res, next) => {
    const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });
    if (!doc) {
        return next(new appError("document with this ID not Exist", 404));
    }
    res.status(200).json({
        updatedDoc: doc,
    });
});

// create doc 
exports.createOne = Model => catchAsync(async (req, res, next) => {
    const doc = new Model(req.body);
    const newDoc = await doc.save();
    res.status(201).json({
        tour: newDoc,
    });
});

// get doc 
exports.getOne = (Model, popOptions) => catchAsync(async (req, res, next) => {
    let query = Model.findById(req.params.id)
    if (popOptions) query = query.populate(popOptions);
    const doc = await query;

    if (!doc) {
        return next(new appError("document with this ID not Exist", 404));
    }
    return res.status(200).json({
        doc: doc,
    });
});

// get all docs 
exports.getAll = (Model) => catchAsync(async (req, res, next) => {
    let filter = {};
    if (req.params.tourId) filter = { tour: req.params.tourId }
    // executing query
    const features = new ApiFeatuers(Model.find(filter), req.query)
        .filter()
        .sort()
        .selectingFields()
        .limitingDocuments();
    const docs = await features.query;

    // sending response
    res.status(200).json({
        result: docs.length,
        docs: docs,
    });
});