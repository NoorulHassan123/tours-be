const User = require("../models/userModel");
const catchAsync = require("../utils/catchAsync");
const appError = require("../utils/appError");
const factory = require("./handlerFactory");

const filterObj = (obj, ...alowedFields) => {
  const newObj = {};
  Object.keys(obj).forEach((el) => {
    if (alowedFields.includes(el)) newObj[el] = obj[el];
  });
  return newObj;
};

exports.getMe = (req, res, next) => {
  req.params.id = req.user.id;
  next();
}

// update user data
exports.updateMe = catchAsync(async (req, res, next) => {
  console.log("file", req.files)

  if (req.body.password || req.body.confirmPassword) {
    return next(
      new appError(
        "This route is not for password updates.please use /updatePassword",
        400
      )
    );
  }

  // const filterdBody = filterObj(req.body, "name", "email");
  // if (req.file) filterdBody.photo = req.file.filename

  const userData = {
    name: req.body.name,
    phone: req.body.phone
    // profileImage: req.files[0].location,
  }

  if (req.files) {
    req.body.photo = req.files[0].location;
  }

  const updatedUser = await User.findByIdAndUpdate(req.user.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    status: "success",
    data: {
      user: updatedUser,
    },
  });
});

// delete user
exports.deleteMe = catchAsync(async (req, res, next) => {
  const deletedUser = await User.findByIdAndUpdate(req.user.id, {
    active: false,
  });
  res.status(204).json({
    status: "success",
  });
});

// get all users

exports.createUser = (req, res) => {
  res.status(201).json({
    message: "user created",
  });
};

// get all users 
exports.getAllUsers = factory.getAll(User);

// for admin 
exports.getUser = factory.getOne(User);
exports.deleteUser = factory.deleteOne(User);
exports.updateUser = factory.updateOne(User);

