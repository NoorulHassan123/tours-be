const toursData = require("../toursData/tours");
const Tour = require("../models/tourModel");
const Booking = require("../models/bookingModel");
const catchAsync = require("../utils/catchAsync");
const appError = require("../utils/appError");

const factory = require("./handlerFactory");
const tours = toursData;

// middleware for checking req.body is valid for creating the event
// exports.checkBody = (req, res, next) => {
//     if (!req.body.place || !req.body.fee) {
//         return res.status(404).json({
//             message: "missing place or fee"
//         })
//     }
//     next();
// }

// middleare for checking id  of tour
// exports.checklId = (req, res, next, val) => {
//     console.log(`the id is:${val}`);
//     const tour = tours.filter((elem) => {
//         return elem.id == req.params.id
//     })
//     if (tour.length == 0) {
//         return res.status(404).json({
//             tour: "invalid id"
//         })
//     }
//     next();
// }

// get top 5 cheap tours
exports.aliasTopTours = catchAsync(async (req, res, next) => {
  req.query.sort = "-fee";
  req.query.limit = "5";
  req.query.fields = "place,fee,maxGroupSize";
  next();
});

// get all tours
exports.getAllTours = factory.getAll(Tour);
// create tour
exports.addTour = factory.createOne(Tour);

// get tour
exports.getTour = factory.getOne(Tour, { path: "reviews" })

// update tour
exports.upDateTour = factory.updateOne(Tour);

// delete tour
exports.deleteTour = factory.deleteOne(Tour)

//get tours stats aggregation
exports.getTourStats = catchAsync(async (req, res, next) => {
  const stats = await Tour.aggregate([
    {
      $match: { fee: { $gte: 5000 } },
    },
    {
      $group: {
        _id: { $toUpper: "$place" },
        numTours: { $sum: 1 },
        totolratingsQuantity: { $sum: "$ratingsQuantity" },
        avgFee: { $avg: "$fee" },
        minFee: { $min: "$fee" },
        maxPrice: { $max: "$fee" },
      },
    },
    {
      $sort: { avgFee: 1 },
    },
    // {
    //   $match: { _id: { $ne: "KAGAN" } },
    // },
  ]);
  res.status(200).json({
    result: "SUCCESS",
    data: stats,
  });
});


// getBookedTours 
exports.getMyTours = catchAsync(async (req, res, next) => {
  const booking = await Booking.find({ user: req.user.id });
  const tourIds = booking.map(el => el.tour);
  const bookedTours = await Tour.find({ _id: { $in: tourIds } });

  res.status(200).json({
    status: 'Sucess',
    result: bookedTours.length,
    data: bookedTours,
  });
});