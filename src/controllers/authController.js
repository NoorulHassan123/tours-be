const User = require("../models/userModel");
const jwt = require("jsonwebtoken");
const catchAsync = require("../utils/catchAsync");
const appError = require("../utils/appError");
const sendEmail = require("../utils/email");
const { promisify } = require("util");
const crypto = require("crypto");
const jwt_decode = require("jwt-decode");
const Email = require("../utils/email");

// create and send token
const cookieOptions = {
  expires: new Date(
    Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
  ),
  httpOnly: true,
};

const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id);
  res.cookie("jwt", token, cookieOptions);
  user.password = undefined;
  res.status(statusCode).json({
    status: "success",
    token,
    user: user,
  });
  let decoded = jwt_decode(token);
  console.log("decoded", decoded);
};

// creating token
const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

// user signup
exports.signUp = catchAsync(async (req, res, next) => {
  const newUser = await User.create(req.body);
  await new Email(newUser).sendWelcome();
  createSendToken(newUser, 201, res);
});

// creat social user
exports.createSocialUser = catchAsync(async (req, res, next) => {
  console.log('body', req.body)
  let user;
  const userRes = await User.findOne({ socialUserId: req.body.socialUserId });
  console.log('userRes', userRes)
  if (!userRes) {
    user = await User.create(req.body);
    await new Email(user).sendWelcome();
    createSendToken(user, 201, res);
  } else {
    user = userRes;
  }

  createSendToken(user, 200, res);
  // res.status(200).json({ status: "success", data: { user } });

});

// user login
exports.logIn = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return next(new appError("plz provide email and password", 400));
  }
  const user = await User.findOne({ email }).select("+password");
  if (!user || !(await user.correctPassword(password, user.password))) {
    return next(new appError("incorrect email or password", 401));
  }
  createSendToken(user, 200, res);
});

// verification jwt
exports.protect = catchAsync(async (req, res, next) => {
  console.log(req.headers);
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    return next(new appError("your are not logged in", 401));
  }
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
  const user = await User.findById(decoded.id);
  if (!user) {
    return next(
      new appError("User belonging to this token does not exist", 401)
    );
  }
  req.user = user;
  next();
});

// restrication
exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new appError("you have no access to perform this action", 402)
      );
    }
    next();
  };
};

// forgot password
exports.forgotPassword = catchAsync(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return next(new appError("There is no user with this emaiL address", 404));
  }
  const resetToken = await user.randomPasswordResetToken();

  await user.save({ validateBeforeSave: false });

  const resetUrl = `${process.env.SUCCESS_PAYMENT_URL}resetPassword/${resetToken}`;

  const message = `forgot your password? submit a patch request with new password and confirmPassword to: ${resetUrl}.\n if you did't forgot your password please ignore this email!`;

  try {
    await new Email(user, resetUrl).passwordReset();
    res.status(200).json({
      status: "success",
      message: "reset token sent to your email",
    });
  } catch (error) {
    user.passwordResetToken = undefined;
    passwordResetExpires = undefined;
    await user.save({ validateBeforeSave: false });
    return new appError(
      "There was an error while sending the email,try again lator",
      500
    );
  }
});

// reset password
exports.resetPassword = catchAsync(async (req, res, next) => {
  // 1.get user based on the token
  const hashPassword = crypto
    .createHash("sha256")
    .update(req.params.token)
    .digest("hex");

  const user = await User.findOne({
    passwordResetToken: hashPassword,
    passwordResetExpires: { $gte: Date.now() },
  });
  // 2.if token has not expired and there is user then set the new password
  if (!user) {
    return next(new appError("Token is invalid or has expired", 400));
  }
  user.password = req.body.password;
  user.confirmPassword = req.body.confirmPassword;
  user.passwordResetExpires = undefined;
  user.passwordResetToken = undefined;
  await user.save();

  // 3.user log in and send jwt
  createSendToken(user, 200, res);
});

// update password
exports.updatePassword = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.user.id).select("+password");
  if (!(await user.correctPassword(req.body.currentPassword, user.password))) {
    return next(new appError("your current password is incorrect", 401));
  }
  user.password = req.body.password;
  user.confirmPassword = req.body.confirmPassword;
  await user.save();
  createSendToken(user, 200, res);
});
