const express = require("express");
const Router = express.Router();
const viewController = require("../controllers/viewsController");

// client side routes 
Router.get("/", viewController.getOverview);

Router.get("/tour", viewController.getTour);

module.exports = Router;