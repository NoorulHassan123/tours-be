const mongoose = require("mongoose");
require("dotenv").config({ path: `${__dirname}/../.env.${process.env.NODE_ENV}` });
// dotenv.config({ path: "./config.env" });
console.log("process.env", process.env.DB_URL, process.env.PORT, process.env.JWT_SECRET, process.env.NODE_ENV)
console.log("dirName", __dirname)
const app = require("./app");

mongoose
  .connect(process.env.DB_URL, {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("connection successful");
  });

const PORT = process.env.PORT || 8000;
const server = app.listen(PORT, () => {
  console.log("app is listen at localhost:5000");
});

process.on("unhandledRejection", (err) => {
  console.log(err);
  console.log("unhandled rejection! shuttingg down");
  server.close(() => {
    process.exit(1);
  });
});
